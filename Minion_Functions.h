//###########################################################################
//
// FILE:   WASP_Functions.h
//
// TITLE:  Prototypes and Definitions for WASP_Minion Application Support Functions
//
//
//###########################################################################
// Date: January 13, 2014 $
//###########################################################################

#ifndef WASP_FUNCTIONS_H_
#define WASP_FUNCTIONS_H_

#define		ADC_RANGE_HI						4095.00
#define		AMPLIFIER_DATA						"AD"
#define		VERSION_RETURN						"VR"
#define		HEATER_CONTROL						"HC"
#define		HEATER_SET							"HS"
#define		HEATER_TEMP							"HT"
#define		LED_COMMAND							"LC"
#define		MOTOR_POSITION						"MP"
#define		SENSOR_VALUE						"SV"
#define		DATA_SEQUENCE						"DS"
#define		AMPLIFIER_CYCLES_LIMIT				1000
#define		AMPLIFIER_TIMEOUT					400000	//400000 corresponds to roughly 1 sec
#define		ANALOG_COUNT						8
#define		FIRMWARE_VERSION					"1.0"
#define		HEATER_COUNT						2
#define		LED_COUNT							24
#define		LED_HALFPERIOD_DEFAULT				13
#define		MOTOR_TIMER_LIMIT					20000
#define		RTD_LIMIT_HI						85.7
#define 	I2C_SLAVE_ADDR        				0x50
#define 	I2C_NUMBYTES          				2
#define 	I2C_EEPROM_HIGH_ADDR  				0x00
#define 	I2C_EEPROM_LOW_ADDR   				0x30

void acquireADC(void);
void get_temperatures(void);
void gpio_config(void);
void adc_config(void);
void heater_init(void);
void initsecureRamFuncs(void);
void led_init(void);
void led_statemachine(void);
signed int max(float num1, float num2);
void sci_parseA(void);
void sci_parseB(void);
Uint32 parse_amplifier(char *command, Uint16 index);
void parse_command(char *command, char *response);
void readDInput(void);
void runMotor(void);
void runPIDLoops(void);
void sci_echoback_init(void);
void sci_fifo_init(void);
void scia_msg(char *msg);
void scia_xmit(int a);
void scib_msg(char *msg);
void scib_xmit(int a);
void selectDyePeriod(Uint16 index);
void setLEDFrequency(void);
void setLEDMask(Uint32 mask);
bool valid_heater(int index);
bool valid_lightMask(Uint32 index);
void validate_sequence_data(void);
void writeDOutput(void);

struct HeaterControl {
	//parameters
	Uint16		timeBase;
	float		cBand;
	float32		setPoint;
	float32		kP;
	float32		kI;

	//inputs
	float32		temp;

	//internal use
	Uint16		timeBaseCount;
	float32		error;
	float32		data1;
	float32		data2;
	float32		controlOut;

	//outputs
	bool		dOut;
};

struct LightControl {
	//parameters
	bool		enableLED;

	//internal use
	Uint16		pulseCount;

	//outputs
	bool		dOut;
};

extern unsigned int secureRamFuncs_loadstart;
extern unsigned int secureRamFuncs_loadsize;
extern unsigned int secureRamFuncs_runstart;
extern bool amplifierSelect, ledTicker, ledToggle, charTriggerA, charTriggerB, timer0Trigger, timer1Trigger, timer2Trigger, amplifierReadTrigger;
extern bool analogAverageTrigger, analogWriteTrigger, motorPosition0, motorPosition1, motorPosition2, motorEnable, motorDirection;
extern bool setMotorPosition, writeMotorPosition;
extern Uint16 charCountA, charCountB, receivedChar, stateValue, delaycount, voltRaw0, countPulses, pulseHalfPeriod;
extern Uint16 analogCycles, analogCycleCount, analogWriteCount, dataCycles, motorPosition, motorPositionActual, tempInt, tempFrac;
extern Uint32 lightsMask, motorTimer, periodDye1, periodDye2, periodDye3;
extern Uint32 systemAnalog[ANALOG_COUNT+1], amplifierAnalog[LED_COUNT+1];
extern char stringInA[25], stringInB[100], stringOut[100];
extern struct HeaterControl systemHeaters[HEATER_COUNT+1];
extern struct LightControl systemLights[LED_COUNT+1];
extern struct I2CMSG I2cMsgOut1;
extern struct I2CMSG I2cMsgIn1;
extern struct I2CMSG *CurrentMsgPtr;

#endif /* WASP_FUNCTIONS_H_ */
