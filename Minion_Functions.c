//###########################################################################
//
// FILE:	WASP_Functions.c
//
// TITLE:	WASP_Minion Application Support Functions.
//
//###########################################################################
// Date: January 13, 2014 $
//###########################################################################
#include "DSP28x_Project.h"     // Device Headerfile and Examples Include File
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include "Minion_Functions.h"

bool amplifierSelect, ledTicker, ledToggle, charTriggerA, charTriggerB,timer0Trigger, timer1Trigger, timer2Trigger, amplifierReadTrigger;
bool analogAverageTrigger, analogWriteTrigger, motorPosition0, motorPosition1, motorPosition2, motorEnable, motorDirection;
bool setMotorPosition, writeMotorPosition;
Uint16 charCountA, charCountB, receivedChar, stateValue, delayCount, voltRaw0, countPulses, pulseHalfPeriod, adcIndex, ledIndex;
Uint16 analogCycles, analogCycleCount, analogWriteCount, dataCycles, motorPosition, motorPositionActual ,tempInt, tempFrac;
Uint32 lightsMask, motorTimer, stateDelayCount, periodDye1, periodDye2, periodDye3;
Uint32 systemAnalog[ANALOG_COUNT+1], amplifierAnalog[LED_COUNT+1];
char stringInA[25], stringInB[100], stringOut[100];
struct HeaterControl systemHeaters[HEATER_COUNT+1];
struct LightControl systemLights[LED_COUNT+1];
// Global variables
// Two bytes will be used for the outgoing address,
// thus only setup 14 bytes maximum
struct I2CMSG I2cMsgOut1={I2C_MSGSTAT_SEND_WITHSTOP,
                          I2C_SLAVE_ADDR,
                          I2C_NUMBYTES,
                          I2C_EEPROM_HIGH_ADDR,
                          I2C_EEPROM_LOW_ADDR,
                          0x12,                   // Msg Byte 1
                          0x34};                  // Msg Byte 2

struct I2CMSG I2cMsgIn1={ I2C_MSGSTAT_SEND_NOSTOP,
                          I2C_SLAVE_ADDR,
                          I2C_NUMBYTES,
                          I2C_EEPROM_HIGH_ADDR,
                          I2C_EEPROM_LOW_ADDR};

struct I2CMSG *CurrentMsgPtr;


// Configure ADC
void adc_config(void)
{
	EALLOW;
		AdcRegs.ADCSOC0CTL.bit.CHSEL 	= 0;    // set SOC0 channel select to AIN_WELL1 (ADCINA0)
		AdcRegs.ADCSOC0CTL.bit.TRIGSEL 	= 1;    // set SOC0 start trigger on TINT0, due to round-robin SOC0 converts first then SOC1
		AdcRegs.ADCSOC0CTL.bit.ACQPS 	= 6;	// set SOC0 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1)

		AdcRegs.ADCSOC1CTL.bit.CHSEL 	= 1;    // set SOC1 channel select to AIN_WELL2 (ADCINA1)
		AdcRegs.ADCSOC1CTL.bit.TRIGSEL 	= 1;    // set SOC1 start trigger on TINT0, due to round-robin SOC0 converts first then SOC1
		AdcRegs.ADCSOC1CTL.bit.ACQPS 	= 6;	// set SOC1 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1)

		AdcRegs.ADCCTL2.bit.ADCNONOVERLAP = 1;	// Enable non-overlap mode, i.e, disallow simultaneous sampling and conversion
		AdcRegs.ADCSOC8CTL.bit.CHSEL 	= 8;    // set SOC8 channel select to AIN_TEMP_MAIN (ADCINB0)
		AdcRegs.ADCSOC8CTL.bit.TRIGSEL 	= 1;    // set SOC8 start trigger on TINT0, due to round-robin SOC0 converts first then SOC1
		AdcRegs.ADCSOC8CTL.bit.ACQPS 	= 6;	// set SOC8 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1)

		AdcRegs.ADCSOC9CTL.bit.CHSEL 	= 9;    // set SOC9 channel select to AIN_TEMP_COVER (ADCINB1)
		AdcRegs.ADCSOC9CTL.bit.TRIGSEL 	= 1;    // set SOC9 start trigger on TINT0, due to round-robin SOC0 converts first then SOC1
		AdcRegs.ADCSOC9CTL.bit.ACQPS 	= 6;	// set SOC9 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1)
	EDIS;
}

//Copy converted values from ADCResult structures
void acquireADC(void)
{
	// Analog Channel 1
	analogCycleCount++;
	systemAnalog[1] += AdcResult.ADCRESULT0;
	systemAnalog[2] += AdcResult.ADCRESULT1;
	systemAnalog[3] += AdcResult.ADCRESULT2;
	systemAnalog[4] += AdcResult.ADCRESULT3;
	systemAnalog[5] += AdcResult.ADCRESULT4;
	systemAnalog[6] += AdcResult.ADCRESULT5;
	systemAnalog[7] += AdcResult.ADCRESULT6;
	systemAnalog[8] += AdcResult.ADCRESULT7;
	if (!analogAverageTrigger && (analogCycleCount >= analogCycles))
		analogAverageTrigger = true;

	// Main Heater Temperature
	voltRaw0 = AdcResult.ADCRESULT8;
	systemHeaters[1].temp = (voltRaw0/(ADC_RANGE_HI)*RTD_LIMIT_HI);

	// Cover Heater Temperature
	systemHeaters[2].temp = (AdcResult.ADCRESULT9/(ADC_RANGE_HI)*RTD_LIMIT_HI);
}

void get_temperatures(void)
{
	tempInt = systemHeaters[1].temp;
	tempFrac = (systemHeaters[1].temp - tempInt*1.0)*100.0;
}

void gpio_config(void)
{
	// Configure designated pins as GPIO input or output pins
	EALLOW;
		//DIN_MOTORPOSITION_0: motor home position
		GpioCtrlRegs.GPBMUX2.bit.GPIO50 = 0; GpioCtrlRegs.GPBDIR.bit.GPIO50 = 0;

		//DIN_MOTORPOSITION_1: motor first position
		GpioCtrlRegs.GPBMUX2.bit.GPIO51 = 0; GpioCtrlRegs.GPBDIR.bit.GPIO51 = 0;


		//DOUT_LED_22: controls LED 22
		GpioCtrlRegs.GPAMUX1.bit.GPIO0 = 0; GpioCtrlRegs.GPADIR.bit.GPIO0 = 1;

		//DOUT_LED_16: controls LED 16
		GpioCtrlRegs.GPAMUX1.bit.GPIO1 = 0; GpioCtrlRegs.GPADIR.bit.GPIO1 = 1;

		//DOUT_LED_21: controls LED 21
		GpioCtrlRegs.GPAMUX1.bit.GPIO2 = 0; GpioCtrlRegs.GPADIR.bit.GPIO2 = 1;

		//DOUT_LED_15: controls LED 15
		GpioCtrlRegs.GPAMUX1.bit.GPIO3 = 0; GpioCtrlRegs.GPADIR.bit.GPIO3 = 1;

		//DOUT_LED_20: controls LED 20
		GpioCtrlRegs.GPAMUX1.bit.GPIO4 = 0; GpioCtrlRegs.GPADIR.bit.GPIO4 = 1;

		//DOUT_LED_14: controls LED 14
		GpioCtrlRegs.GPAMUX1.bit.GPIO5 = 0; GpioCtrlRegs.GPADIR.bit.GPIO5 = 1;

		//DOUT_LED_19: controls LED 19
		GpioCtrlRegs.GPAMUX1.bit.GPIO6 = 0; GpioCtrlRegs.GPADIR.bit.GPIO6 = 1;

		//DOUT_LED_13: controls LED 13
		GpioCtrlRegs.GPAMUX1.bit.GPIO7 = 0; GpioCtrlRegs.GPADIR.bit.GPIO7 = 1;

		//DOUT_LED_18: controls LED 18
		GpioCtrlRegs.GPAMUX1.bit.GPIO8 = 0; GpioCtrlRegs.GPADIR.bit.GPIO8 = 1;

		//DOUT_LED_12: controls LED 12
		GpioCtrlRegs.GPAMUX1.bit.GPIO9 = 0; GpioCtrlRegs.GPADIR.bit.GPIO9 = 1;

		//DOUT_LED_17: controls LED 17
		GpioCtrlRegs.GPAMUX1.bit.GPIO10 = 0; GpioCtrlRegs.GPADIR.bit.GPIO10 = 1;

		//DOUT_LED_11: controls LED 11
		GpioCtrlRegs.GPAMUX1.bit.GPIO11 = 0; GpioCtrlRegs.GPADIR.bit.GPIO11 = 1;

		//DOUT_LED_08: controls LED 08
		GpioCtrlRegs.GPAMUX1.bit.GPIO13 = 0; GpioCtrlRegs.GPADIR.bit.GPIO13 = 1;

		//DOUT_LED_07: controls LED 07
		GpioCtrlRegs.GPAMUX1.bit.GPIO14 = 0; GpioCtrlRegs.GPADIR.bit.GPIO14 = 1;

		//THORAX_SELECT: selects the first or the second amplifier board
		GpioCtrlRegs.GPAMUX2.bit.GPIO16 = 0; GpioCtrlRegs.GPADIR.bit.GPIO16 = 1;

		//DOUT_LED_04: controls LED 04
		GpioCtrlRegs.GPAMUX2.bit.GPIO17 = 0; GpioCtrlRegs.GPADIR.bit.GPIO17 = 1;

		//DOUT_LED_03: controls LED 03
		GpioCtrlRegs.GPAMUX2.bit.GPIO19 = 0; GpioCtrlRegs.GPADIR.bit.GPIO19 = 1;

		//DOUT_LED_02: controls LED 02
		GpioCtrlRegs.GPAMUX2.bit.GPIO21 = 0; GpioCtrlRegs.GPADIR.bit.GPIO21 = 1;

		//DOUT_LED_01: controls LED 01
		GpioCtrlRegs.GPAMUX2.bit.GPIO23 = 0; GpioCtrlRegs.GPADIR.bit.GPIO23 = 1;

		//DOUT_LED_06: controls LED 06
		GpioCtrlRegs.GPAMUX2.bit.GPIO25 = 0; GpioCtrlRegs.GPADIR.bit.GPIO25 = 1;

		//DOUT_MOTOR_ENABLE: enables/disables motor
		GpioCtrlRegs.GPAMUX2.bit.GPIO26 = 0; GpioCtrlRegs.GPADIR.bit.GPIO26 = 1;

		//DOUT_LED_05: controls LED 05
		GpioCtrlRegs.GPAMUX2.bit.GPIO27 = 0; GpioCtrlRegs.GPADIR.bit.GPIO27 = 1;

		//DOUT_HEATER_MAIN: PWM for Main Heater
		GpioCtrlRegs.GPAMUX2.bit.GPIO31 = 0; GpioCtrlRegs.GPADIR.bit.GPIO31 = 1;

		//DOUT_LED_BOARD: toggles at 500ms indicating normal program execution
		GpioCtrlRegs.GPBMUX1.bit.GPIO34 = 0; GpioCtrlRegs.GPBDIR.bit.GPIO34 = 1;

		//DOUT_LED_10: controls LED 10
		GpioCtrlRegs.GPBMUX1.bit.GPIO41 = 0; GpioCtrlRegs.GPBDIR.bit.GPIO41 = 1;

		//DOUT_LED_09: controls LED 09
		GpioCtrlRegs.GPBMUX1.bit.GPIO43 = 0; GpioCtrlRegs.GPBDIR.bit.GPIO43 = 1;

		//DOUT_LED_24: controls LED 24
		GpioCtrlRegs.GPBMUX2.bit.GPIO54 = 0; GpioCtrlRegs.GPBDIR.bit.GPIO54 = 1;

		//DOUT_LED_23: controls LED 23
		GpioCtrlRegs.GPBMUX2.bit.GPIO56 = 0; GpioCtrlRegs.GPBDIR.bit.GPIO56 = 1;

		//DOUT_HEATER_COVER: PWM for Cover Heater
		GpioCtrlRegs.GPBMUX2.bit.GPIO58 = 0; GpioCtrlRegs.GPBDIR.bit.GPIO58 = 1;
	EDIS;
}

// Initialize heater values
void heater_init(void)
{
	Uint16 i = 0;
	for (i = 1; i <= HEATER_COUNT; i++)
	{
		systemHeaters[i].timeBase = 10000;
		systemHeaters[i].cBand = 2.0;
		systemHeaters[i].setPoint = 0;
		systemHeaters[i].kP = 12;
		systemHeaters[i].kI = 0.0001;
	}
}

// Copy the secureRamFuncs section
void initsecureRamFuncs(void)
{
	memcpy(&secureRamFuncs_runstart,
	&secureRamFuncs_loadstart,
	(Uint32)&secureRamFuncs_loadsize);
}

// Initialize LED values
void led_init(void)
{
	Uint16 i = 0;
	pulseHalfPeriod = LED_HALFPERIOD_DEFAULT;
	for (i = 1; i <= LED_COUNT; i++)
	{
		systemLights[i].enableLED = false;
	}
}

// LED State Machine
void led_statemachine(void)
{
	Uint32 i;

	if (stateValue == 10)
	{
		ledIndex = 0;
		led_init();
		stateValue = 20;
	}
	else if (stateValue == 20)
	{
		i = 1; i = i << ledIndex;
		ledIndex++; amplifierSelect = (ledIndex > 16);
		selectDyePeriod(ledIndex);
		amplifierAnalog[ledIndex] = 2; //value of 2 indicates timeout from amplifier
		adcIndex = ledIndex % ANALOG_COUNT;
		if (adcIndex == 0)
			adcIndex = 8;

		setLEDMask(i);
		stateValue = 30; stateDelayCount = 0;
	}
	else if (stateValue == 30)
	{
		amplifierReadTrigger = true;
		sprintf(stringOut, AMPLIFIER_DATA",%d,%d\r\n\0", dataCycles, adcIndex);
		scib_msg(stringOut);
		stateValue = 40; stateDelayCount = 0;
	}
	else if (stateValue == 40)
	{
		stateDelayCount++;
		if ((!amplifierReadTrigger) || (stateDelayCount >= AMPLIFIER_TIMEOUT))
			stateValue = 50;
	}
	else if (stateValue == 50)
	{
		if (!amplifierReadTrigger)
			amplifierAnalog[ledIndex] = parse_amplifier(stringInB, adcIndex);
		if (ledIndex == LED_COUNT)
			stateValue = 60;
		else
			stateValue = 20;
	}
	else if (stateValue == 60)
	{
		//Add logic to return complete ADC string to calling PC
		setLEDMask(0);
		scia_msg(DATA_SEQUENCE);
		analogWriteCount = 0;
		stateValue = 70;
	}
	else if (stateValue == 70)
	{
		analogWriteCount++;
		sprintf(stringOut, ",%ld", amplifierAnalog[analogWriteCount]);
		scia_msg(stringOut);
		if (analogWriteCount >= LED_COUNT)
			stateValue = 80;
	}
	else if (stateValue == 80)
	{
		sprintf(stringOut, ",%d.%d\r\n\0", tempInt, tempFrac);
		scia_msg(stringOut);
		stateValue = 0;
	}
}

Uint32 parse_amplifier(char *command, Uint16 index)
{
	//Add logic to parse ADC string
	Uint32 value;

	strtok(command, ",");
	value = atol(strtok(NULL, ","));
	return value;
}

void parse_command(char *command, char *response)
{
	Uint16 heaterIndex;

	strncpy(response, "ERROR\r\n\0", 11);
	if (strncmp(command, HEATER_SET, 2) == 0)
	{
		strtok(command, ",");
		heaterIndex = atoi(strtok(NULL, ","));
		if (valid_heater(heaterIndex))
		{
			systemHeaters[heaterIndex].setPoint = atof(strtok(NULL, ","));
			strncpy(response, HEATER_SET"\r\n\0", 5);
		}
	}
	else if (strncmp(command, HEATER_CONTROL, 2) == 0)
	{
		strtok(command, ",");
		heaterIndex = atoi(strtok(NULL, ","));
		if (valid_heater(heaterIndex))
		{
			systemHeaters[heaterIndex].kP = atof(strtok(NULL, ","));
			systemHeaters[heaterIndex].kI = atof(strtok(NULL, ","));
			systemHeaters[heaterIndex].cBand = atof(strtok(NULL, ","));
			systemHeaters[heaterIndex].timeBase = atof(strtok(NULL, ","));
			strncpy(response, HEATER_CONTROL"\r\n\0", 5);
		}
	}
	else if (strncmp(command, HEATER_TEMP, 2) == 0)
	{
		strtok(command, ",");
		heaterIndex = atoi(strtok(NULL, ","));
		if (valid_heater(heaterIndex))
		{
			tempInt = (Uint16)systemHeaters[heaterIndex].temp;
			sprintf(response, HEATER_TEMP",%d.%d\r\n\0", tempInt, tempFrac);
		}
	}
	else if (strncmp(command, LED_COMMAND, 2) == 0)
	{
		strtok(command, ",");
		lightsMask = atol(strtok(NULL, ","));
		if (valid_lightMask(lightsMask))
		{
			pulseHalfPeriod = atoi(strtok(NULL, ","));
			setLEDMask(lightsMask);
			sprintf(response, LED_COMMAND",%ld\r\n\0", lightsMask);
		}
	}
	else if (strncmp(command, MOTOR_POSITION, 2) == 0)
	{
		strtok(command, ",");
		motorPosition = atoi(strtok(NULL, ","));
		if (motorPosition < 3)
		{
			setMotorPosition = true;
			response[0] = '\0';
		}
	}
	else if (strncmp(command, SENSOR_VALUE, 2) == 0)
	{
		strtok(command, ",");
		analogCycles = atoi(strtok(NULL, ","));
		if (analogCycles > 0)
		{
			analogCycleCount = 0; analogWriteTrigger = false; analogAverageTrigger = false;
			systemAnalog[1] = 0; systemAnalog[2] = 0; systemAnalog[3] = 0; systemAnalog[4] = 0;
			systemAnalog[5] = 0; systemAnalog[6] = 0; systemAnalog[7] = 0; systemAnalog[8] = 0;
			response[0] = '\0';
		}
	}
	else if (strncmp(command, DATA_SEQUENCE, 2) == 0)
	{
		strtok(command, ",");
		dataCycles = atoi(strtok(NULL, ","));
		periodDye1 = atol(strtok(NULL, ","));
		periodDye2 = atol(strtok(NULL, ","));
		periodDye3 = atol(strtok(NULL, ","));
		validate_sequence_data();
		stateValue = 10;
		response[0] = '\0';
	}
	else if (strncmp(command, VERSION_RETURN, 2) == 0)
	{
		sprintf(response, VERSION_RETURN",%s\r\n\0", FIRMWARE_VERSION);
	}
}

/* function returning the max between two numbers */
signed int max(float num1, float num2)
{
   if (num1 >= num2)
      return num1;
   return num2;
}

void readDInput(void)
{
	motorPosition0 = GpioDataRegs.GPBDAT.bit.GPIO50;
	motorPosition1 = GpioDataRegs.GPBDAT.bit.GPIO51;
	motorPosition2 = GpioDataRegs.GPBDAT.bit.GPIO54;
}

// Execute Motor Positioning
void runMotor(void)
{
	if (setMotorPosition)
	{
		motorTimer++;
		if (motorPosition == 0)
		{
			motorEnable = 1; motorDirection = 0;
			if (motorPosition0)
			{
				setMotorPosition = false; writeMotorPosition = true;
				motorPositionActual = 0;
			}
		}
		if (motorPosition == 1)
		{
			motorEnable = 1; motorDirection = 1;
			if (motorPosition1)
			{
				setMotorPosition = false; writeMotorPosition = true;
				motorPositionActual = 1;
			}
		}
		if (motorPosition == 2)
		{
			motorEnable = 1; motorDirection = 1;
			if (motorPosition2)
			{
				setMotorPosition = false; writeMotorPosition = true;
				motorPositionActual = 2;
			}
		}
		if (motorTimer >= MOTOR_TIMER_LIMIT)
		{
			setMotorPosition = false; writeMotorPosition = true;
			motorPositionActual = 100;
		}
	}
	else
	{
		motorTimer = 0, motorEnable = 0;
	}
}

// Execute PID Loops
void runPIDLoops(void)
{
	Uint16 i = 0;
	for (i = 1; i <= HEATER_COUNT; i++)
	{
		if (systemHeaters[i].setPoint > 0)
		{
			//Calculate and accumulate the Error
			systemHeaters[i].error = systemHeaters[i].setPoint - systemHeaters[i].temp;
			systemHeaters[i].data1 = systemHeaters[i].error * systemHeaters[i].kI;
			systemHeaters[i].data2 += systemHeaters[i].data1;
			//Limit the maximum and minimum values of accumulated error
			if	(systemHeaters[i].data2 >= 100.0) systemHeaters[i].data2 = 100.0;
			if	(systemHeaters[i].data2 <= 0.0) systemHeaters[i].data2 = 0.0;

			//Disable Integral and Derivative Control if out of Proportional band
			if (fabsf(systemHeaters[i].error) > systemHeaters[i].cBand) { systemHeaters[i].data1 = 0; systemHeaters[i].data2 = 0;}

			//Calculate controlOut
			systemHeaters[i].controlOut = systemHeaters[i].error * systemHeaters[i].kP + systemHeaters[i].data2;

			//Limit the maximum and minimum values of controlOut
			if (systemHeaters[i].controlOut >= 100.0) systemHeaters[i].controlOut = 100.0;
			if (systemHeaters[i].controlOut <= 0.0) systemHeaters[i].controlOut = 0.0;

			//Apply PWM to controlOut
			systemHeaters[i].timeBaseCount = (systemHeaters[i].timeBaseCount + (Uint16)systemHeaters[i].controlOut) % systemHeaters[i].timeBase;
			systemHeaters[i].dOut = (systemHeaters[i].timeBaseCount < (0.01*systemHeaters[i].controlOut*systemHeaters[i].timeBase));
		}
		else
		{
			systemHeaters[i].data2 = 0; systemHeaters[i].controlOut = 0.0; systemHeaters[i].dOut = 0;
		}
	}

}

// Test 1,SCIA  DLB, 8-bit word, baud rate 0x0103, default, 1 STOP bit, no parity
void sci_echoback_init(void)
{
    // Note: Clocks were turned on to the SCIA peripheral
    // in the InitSysCtrl() function

 	SciaRegs.SCICCR.all =0x0007;   // 1 stop bit,  No loopback
                                   // No parity,8 char bits,
                                   // async mode, idle-line protocol
	SciaRegs.SCICTL1.all =0x0003;  // enable TX, RX, internal SCICLK,
                                   // Disable RX ERR, SLEEP, TXWAKE

	SciaRegs.SCICTL2.bit.TXINTENA =1;
	SciaRegs.SCICTL2.bit.RXBKINTENA =1;

	SciaRegs.SCIHBAUD    =0x0001;  // 9600 baud @LSPCLK = 20MHz (80 MHz SYSCLK).
    SciaRegs.SCILBAUD    =0x0003;

	SciaRegs.SCICTL1.all =0x0023;  // Relinquish SCI from Reset

	ScibRegs.SCICCR.all =0x0007;   // 1 stop bit,  No loopback
	                          	  	  // No parity,8 char bits,
	                            	// async mode, idle-line protocol
	ScibRegs.SCICTL1.all =0x0003;  	// enable TX, RX, internal SCICLK,
	   	   	   	   	   	   	   	   	   // Disable RX ERR, SLEEP, TXWAKE

	ScibRegs.SCICTL2.bit.TXINTENA =1;
	ScibRegs.SCICTL2.bit.RXBKINTENA =1;

	ScibRegs.SCIHBAUD    =0x0001;  // 9600 baud @LSPCLK = 20MHz (80 MHz SYSCLK).
    ScibRegs.SCILBAUD    =0x0003;

	ScibRegs.SCICTL1.all =0x0023;  // Relinquish SCI from Reset
}

void scia_msg(char * msg)
{
    Uint16 i;
    i = 0;
    while(msg[i] != '\0')
    {
        scia_xmit(msg[i]);
        i++;
    }
}

// Transmit a character from the SCIA
void scia_xmit(int a)
{
	while(SciaRegs.SCICTL2.bit.TXRDY == 0){;}
	SciaRegs.SCITXBUF = a;
}

void scib_msg(char * msg)
{
    Uint16 i;
    i = 0;
    while(msg[i] != '\0')
    {
        scib_xmit(msg[i]);
        i++;
    }
}

// Transmit a character from the SCIB
void scib_xmit(int a)
{
	while(ScibRegs.SCICTL2.bit.TXRDY == 0){;}
	ScibRegs.SCITXBUF = a;
}

// Initalize the SCI FIFO
void sci_fifo_init(void)
{
	SciaRegs.SCIFFTX.all=0xA000;
	SciaRegs.SCIFFRX.all=0x2000;
    SciaRegs.SCIFFCT.all=0x0;

    ScibRegs.SCIFFTX.all=0xA000;
    ScibRegs.SCIFFRX.all=0x2000;
    ScibRegs.SCIFFCT.all=0x0;
}

// Capture and Parse SCIA
void sci_parseA(void)
{
	if (charTriggerA)
	{
		// Echo character back as soon as end character received
	   	stringOut[0] = '\0';
		scia_msg("!\r\n\0");
		stringInA[max(0,charCountA-1)] = 0;
		parse_command(stringInA, stringOut);
		scia_msg(stringOut);
		ledTicker = true;
		charCountA = 0;
		charTriggerA = 0;
   }
}

// Capture and Parse SCIB
void sci_parseB(void)
{
	if (charTriggerB)
	{
		// Echo character back as soon as end character received
		stringInB[max(0,charCountB-1)] = 0;
		charCountB = 0;
		charTriggerB = 0;
		amplifierReadTrigger = false;
   }
}

void selectDyePeriod(Uint16 index)
{
	if (index <= 8)
		pulseHalfPeriod = periodDye1;
	if ((index > 8) && (index <= 16))
		pulseHalfPeriod = periodDye2;
	if (index > 16)
		pulseHalfPeriod = periodDye3;
}

void setLEDFrequency(void)
{
	Uint16 i = 0;
	countPulses = (countPulses + 1) % pulseHalfPeriod;
	if (pulseHalfPeriod > 0)
	{
		if(countPulses == 0)
		{
			for (i = 1; i <= LED_COUNT; i++)
			{
				systemLights[i].dOut = !(systemLights[i].dOut && systemLights[i].enableLED);
			}
		}
	}
}

void setLEDMask(Uint32 mask)
{
	Uint32 i = 0, j = 1;
	for (i = 1; i <= LED_COUNT; i++)
	{
		systemLights[i].enableLED = ((j & mask) > 0);
		systemLights[i].dOut = !((!pulseHalfPeriod) && (systemLights[i].enableLED));
		j = j << 1;
	}
}

bool valid_heater(int index)
{
	if ((index >= 1)&&(index <= HEATER_COUNT))
		return true;
	else
		return false;
}

bool valid_lightMask(Uint32 index)
{
	if (index <= (8388608L))
		return true;
	else
		return false;
}

void validate_sequence_data(void)
{
	if (dataCycles <= 0)
		dataCycles = 1;
	else if (dataCycles >= AMPLIFIER_CYCLES_LIMIT)
		dataCycles = AMPLIFIER_CYCLES_LIMIT;

	if ((periodDye1 <= 0) || (periodDye1 > 10000000))
		periodDye1 = LED_HALFPERIOD_DEFAULT;

	if ((periodDye2 <= 0) || (periodDye2 > 10000000))
		periodDye2 = LED_HALFPERIOD_DEFAULT;

	if ((periodDye3 <= 0) || (periodDye3 > 10000000))
		periodDye3 = LED_HALFPERIOD_DEFAULT;
}

void writeDOutput(void)
{
	//the delayCount increment instructions insert suitable delay between GPxDAT register writes
	//this prevents overwriting the register in successive instructions
	GpioDataRegs.GPADAT.bit.GPIO0 = systemLights[22].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPADAT.bit.GPIO1 = systemLights[16].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPADAT.bit.GPIO2 = systemLights[21].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPADAT.bit.GPIO3 = systemLights[15].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPADAT.bit.GPIO4 = systemLights[20].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPADAT.bit.GPIO5 = systemLights[14].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPADAT.bit.GPIO6 = systemLights[19].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPADAT.bit.GPIO7 = systemLights[13].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPADAT.bit.GPIO8 = systemLights[18].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPADAT.bit.GPIO9 = systemLights[12].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPADAT.bit.GPIO10 = systemLights[17].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPADAT.bit.GPIO11 = systemLights[11].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPADAT.bit.GPIO13 = systemLights[8].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPADAT.bit.GPIO14 = systemLights[7].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPADAT.bit.GPIO16 = amplifierSelect;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPADAT.bit.GPIO17 = systemLights[4].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPADAT.bit.GPIO19 = systemLights[3].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPADAT.bit.GPIO21 = systemLights[2].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPADAT.bit.GPIO23 = systemLights[1].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPADAT.bit.GPIO25 = systemLights[6].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPADAT.bit.GPIO26 = motorEnable;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPADAT.bit.GPIO27 = systemLights[5].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPADAT.bit.GPIO31 = systemHeaters[1].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPBDAT.bit.GPIO34 = ledToggle;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPBDAT.bit.GPIO41 = systemLights[10].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPBDAT.bit.GPIO43 = systemLights[9].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPBDAT.bit.GPIO54 = systemLights[24].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPBDAT.bit.GPIO56 = systemLights[23].dOut;delayCount++;delayCount++;delayCount++;
	GpioDataRegs.GPBDAT.bit.GPIO58 = systemHeaters[2].dOut;delayCount++;delayCount++;delayCount++;
}

//===========================================================================
// No more.
//===========================================================================
