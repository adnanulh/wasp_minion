//###########################################################################
//
//!  WASP_Minion
//!
//!  This program works as a server that collects digital and analog values
//!  from the GPIO pins and transfers them to the external calling application
//!  over UART. Light-load operations are also performed on some of these I/O
//!	 values such as PWM closed-loop control, scaling and averaging.
//!
//###########################################################################
// Property of Douglas Scientific
//###########################################################################


#include "DSP28x_Project.h"     // Device Headerfile and Examples Include File
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "Minion_Functions.h"

// Prototype statements for functions found within this file.
__interrupt void cpu_timer0_isr(void);
__interrupt void cpu_timer1_isr(void);
__interrupt void cpu_timer2_isr(void);
__interrupt void sci_a_isr(void);
__interrupt void sci_b_isr(void);
__interrupt void i2c_int1a_isr(void);
void task0Run(void);
void task1Run(void);

void main(void)
{
	// Copy the code section secureRamFuncs from the LOAD location to the RUN location, both
	//specified in the selected linker command *.cmd file
	initsecureRamFuncs();
	// Initialize the on-chip flash registers through function InitFlash() //
	// If running from Flash, uncomment the function call below and in the
	// change the linker command file to the Flash version
	//InitFlash();
	// Initialize System Control:
	// PLL, WatchDog, enable Peripheral Clocks
	// This example function is found in the F2806x_SysCtrl.c file.
	InitSysCtrl();

	// Initalize GPIO:
	// This example function is found in the F2806x_Gpio.c file and
	// illustrates how to set the GPIO to it's default state.
	CurrentMsgPtr = &I2cMsgOut1;
	InitGpio();
	InitI2CGpio();

	// For this example, only init the pins for the SCI-A port.
	// This function is found in the Minion_Sci.c file.
	InitSciaGpio();
	InitScibGpio();

	// Clear all interrupts and initialize PIE vector table:
	// Disable CPU interrupts
	DINT;

	// Initialize the PIE control registers to their default state.
	// The default state is all PIE interrupts disabled and flags
	// are cleared.
	// This function is found in the F2806x_PieCtrl.c file.
	InitPieCtrl();

	// Disable CPU interrupts and clear all CPU interrupt flags:
	IER = 0x0000;
	IFR = 0x0000;

	// Initialize the PIE vector table with pointers to the shell Interrupt
	// Service Routines (ISR).
	// This will populate the entire table, even if the interrupt
	// is not used in this example.  This is useful for debug purposes.
	// The shell ISR routines are found in F2806x_DefaultIsr.c.
	// This function is found in F2806x_PieVect.c.
	InitPieVectTable();

	// Interrupts that are used in this example are re-mapped to
	// ISR functions found within this file.
	EALLOW;  // This is needed to write to EALLOW protected registers
		PieVectTable.TINT0 = &cpu_timer0_isr;
		PieVectTable.TINT1 = &cpu_timer1_isr;
		PieVectTable.TINT2 = &cpu_timer2_isr;
		PieVectTable.SCIRXINTA = &sci_a_isr;
		PieVectTable.SCIRXINTB = &sci_b_isr;
		PieVectTable.I2CINT1A = &i2c_int1a_isr;
	EDIS;    // This is needed to disable write to EALLOW protected registers

	// Initialize the Device Peripheral. This function can be
	// found in F2806x_CpuTimers.c
	InitCpuTimers();

	// Configure CPU-Timer 0 to interrupt every 500 milliseconds:
	// 80MHz CPU Freq, 500 millisecond Period (in uSeconds)
	ConfigCpuTimer(&CpuTimer0, 80, 250);
	ConfigCpuTimer(&CpuTimer1, 80, 500000);
	ConfigCpuTimer(&CpuTimer2, 80, 25);

	// Initialize all the Device Peripherals:
	// This function is found in F2806x_InitPeripherals.c
	// InitPeripherals(); // Not required for this example
	InitAdc();  // For this example, init the ADC



	// To ensure precise timing, use write-only instructions to write to the entire register. Therefore, if any
	// of the configuration bits are changed in ConfigCpuTimer and InitCpuTimers (in F2806x_CpuTimers.h), the
	// below settings must also be updated.
	CpuTimer0Regs.TCR.all = 0x4001; // Use write-only instruction to set TSS bit = 0
	CpuTimer1Regs.TCR.all = 0x4001; // Use write-only instruction to set TSS bit = 0
	CpuTimer2Regs.TCR.all = 0x4001; // Use write-only instruction to set TSS bit = 0

	//Set up all GPIO pins
	gpio_config();

	//Initialize heaters
	heater_init();

	//Initialize LEDs
	led_init();

	// User specific code, enable interrupts:
	PieCtrlRegs.PIEIER1.bit.INTx1 = 1;	// Enable ADCINT1 in the PIE: Group 1 interrupt 1
	PieCtrlRegs.PIEIER1.bit.INTx7 = 1;	// Enable TINT0 in the PIE: Group 1 interrupt 7
	PieCtrlRegs.PIEIER8.bit.INTx1 = 1;	// Enable I2CINT1A in the PIE: Group 8 interrupt 1
	PieCtrlRegs.PIEIER9.bit.INTx1 = 1;	// Enable SCIRXINTA in the PIE: Group 9 interrupt 1
	PieCtrlRegs.PIEIER9.bit.INTx3 = 1;	// Enable SCIRXINTB in the PIE: Group 9 interrupt 3


	// Enable CPU INTs
	IER |= M_INT1;	//INT1 connected to ADCINT1, TINT0:
	IER |= M_INT8;	//INT8 connected to I2CINT1A:
	IER |= M_INT9;	//INT9 connected to SCIRXINTA, SCIRXINTB:
	IER |= M_INT13;	//INT13 connected to CPU-Timer 1:
	IER |= M_INT14;	//INT13 connected to CPU-Timer 2:

	// Enable global Interrupts and higher priority real-time debug events:
	EINT;   // Enable Global interrupt INTM
	ERTM;   // Enable Global realtime interrupt DBGM

	//Configure ADC
	adc_config();

   	// App specific code:
   	ledTicker = true; analogAverageTrigger = true; analogWriteTrigger = true;
   	charCountA = 0; charCountB = 0; charTriggerA = false; stringInA[0] = '\0'; stringInB[0] = '\0';
   	setMotorPosition = false; writeMotorPosition = false;
   	sci_fifo_init();	   // Initialize the SCI FIFO
   	sci_echoback_init();  // Initalize SCI for echoback

   	for(;;)
       {
   			sci_parseA();
   			sci_parseB();
   			led_statemachine();

   			if (!analogWriteTrigger && analogAverageTrigger)
   			{
   				sprintf(stringOut, SENSOR_VALUE",%ld,%ld,%ld,%ld,%ld,%ld,%ld,%ld\r\n\0",
   					systemAnalog[1]/analogCycles, systemAnalog[2]/analogCycles, systemAnalog[3]/analogCycles, systemAnalog[4]/analogCycles,
   					systemAnalog[5]/analogCycles, systemAnalog[6]/analogCycles, systemAnalog[7]/analogCycles, systemAnalog[8]/analogCycles);
   				scia_msg(stringOut);
   				analogWriteTrigger = true;
   			}

   			if (writeMotorPosition)
   			{
   				sprintf(stringOut, MOTOR_POSITION",%d\r\n\0", motorPositionActual);
   				scia_msg(stringOut);
   				writeMotorPosition = false;
   			}

   			if (timer0Trigger)
   			{
   				//Add App specific cyclic code
   				task0Run();
   				timer0Trigger = 0;
   			}

   			if (timer1Trigger)
   			{
   				if (ledTicker)
   					ledToggle = !ledToggle;
   				else
   					ledToggle = false;

   				timer1Trigger = 0;
   			}

   			if (timer2Trigger)
   			{
   				task1Run();
   				timer2Trigger = 0;
   			}
       }
}

__interrupt void cpu_timer0_isr(void)
{
	EINT;
	timer0Trigger = 1;
	PieCtrlRegs.PIEACK.bit.ACK1 = 1; // Acknowledge this interrupt to receive more interrupts from group 1
}
__interrupt void cpu_timer1_isr(void)
{
	EINT;
	timer1Trigger = 1;
}
__interrupt void cpu_timer2_isr(void)
{
	EINT;
	timer2Trigger = 1;
}

__interrupt void sci_a_isr(void)
{
	// Get character
	receivedChar = SciaRegs.SCIRXBUF.all;
	charCountA++;
	switch(receivedChar)
	{
		case 10:
		{
			charTriggerA = 1;
		}
		default :
		{
			// Accumulate characters in fin till an end character is received
			stringInA[max(0,charCountA-1)] = receivedChar;
			stringInA[max(0,charCountA)] = 0;
			ledTicker = false;
			break;
		}
	}
	PieCtrlRegs.PIEACK.bit.ACK9 = 1;    // Acknowledge interrupt to PIE
	return;
}

__interrupt void sci_b_isr(void)
{
	// Get character
	receivedChar = ScibRegs.SCIRXBUF.all;
	charCountB++;
	switch(receivedChar)
	{
		case 10:
		{
			charTriggerB = 1;
		}
		default :
		{
			// Accumulate characters in fin till an end character is received
			if (receivedChar == 'E')
				receivedChar = '1';
			stringInB[max(0,charCountB-1)] = receivedChar;
			stringInB[max(0,charCountB)] = 0;
			break;
		}
	}
	PieCtrlRegs.PIEACK.bit.ACK9 = 1;    // Acknowledge interrupt to PIE
	return;
}

__interrupt void i2c_int1a_isr(void)     // I2C-A
{
   //Uint16 IntSource;

   // Read interrupt source
   //IntSource = I2caRegs.I2CISRC.all;

   // Enable future I2C (PIE Group 8) interrupts
   PieCtrlRegs.PIEACK.all = PIEACK_GROUP8;
}

void task0Run(void)
{
	acquireADC();
	readDInput();
	runPIDLoops();
	runMotor();
	get_temperatures();
}

void task1Run(void)
{
	setLEDFrequency();
	writeDOutput();
}

//===========================================================================
// No more.
//===========================================================================
